package com.aryanto.widi.mynoteapps.repository;

import android.app.Application;

import androidx.paging.DataSource;
import androidx.sqlite.db.SimpleSQLiteQuery;

import com.aryanto.widi.mynoteapps.database.Note;
import com.aryanto.widi.mynoteapps.database.NoteDao;
import com.aryanto.widi.mynoteapps.database.NoteDatabase;
import com.aryanto.widi.mynoteapps.helper.SortUtils;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class NoteRepository {
    private NoteDao mNotesDao;
    private ExecutorService executorService;
    public NoteRepository(Application application) {
        executorService = Executors.newSingleThreadExecutor();
        NoteDatabase db = NoteDatabase.getDatabase(application);
        mNotesDao = db.noteDao();
    }

    // public LiveData<List<Note>> getAllNotes() {
    //     return mNotesDao.getAllNotes();
    // }

    // public DataSource.Factory<Integer, Note> getAllNotes() {
    //     return mNotesDao.getAllNotes();
    // }

    public DataSource.Factory<Integer, Note> getAllNotes(String sort) {
        SimpleSQLiteQuery query = SortUtils.getSorteredQuery(sort);
        return mNotesDao.getAllNotes(query);
    }

    public void insert(final Note note) {
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                mNotesDao.insert(note);
            }
        });
    }

    public void delete(final Note note){
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                mNotesDao.delete(note);
            }
        });
    }

    public void update(final Note note){
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                mNotesDao.update(note);
            }
        });
    }
}
