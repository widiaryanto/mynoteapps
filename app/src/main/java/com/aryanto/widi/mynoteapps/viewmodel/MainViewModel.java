package com.aryanto.widi.mynoteapps.viewmodel;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PagedList;

import com.aryanto.widi.mynoteapps.database.Note;
import com.aryanto.widi.mynoteapps.repository.NoteRepository;

public class MainViewModel extends ViewModel {
    private NoteRepository mNoteRepository;

    public MainViewModel(Application application) {
        mNoteRepository = new NoteRepository(application);
    }

    // LiveData<List<Note>> getAllNotes() {
    //     return mNoteRepository.getAllNotes();
    // }

    // LiveData<PagedList<Note>> getAllNotes() {
    //     return new LivePagedListBuilder<>(mNoteRepository.getAllNotes(), 20).build();
    // }

    public LiveData<PagedList<Note>> getAllNotes(String sort) {
        return new LivePagedListBuilder<>(
                mNoteRepository.getAllNotes(sort), 20).build();
    }
}